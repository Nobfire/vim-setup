package main

import (
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path"
)

var (
	homeDir          = os.Getenv(`HOME`)
	vimrcURL         = `https://gitlab.com/Nobfire/vim-setup/-/raw/master/vimrc`
	monokaiURL       = `https://raw.githubusercontent.com/sickill/vim-monokai/master/colors/monokai.vim`
	distinguishedURL = `https://raw.githubusercontent.com/Lokaltog/vim-distinguished/develop/colors/distinguished.vim`
)

func main() {
	createVimrc()
	installVundle()
	installPathogen()
	installColors()
	installPlugins()
}

func createVimrc() {
	// Get our vimrc file
	log.Print(`Creating vimrc file.`)
	vimrcBytes, err := fetchFile(vimrcURL)
	if err != nil {
		log.Fatal(err)
	}

	// Create the vimrc file
	file, err := os.OpenFile(path.Join(homeDir, `.vimrc`), os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0755)
	if err != nil {
		log.Fatal(err)
	}
	_, err = file.Write(vimrcBytes)
	if err != nil {
		log.Fatal(err)
	}
}

func installVundle() {
	// Install Vundle
	log.Print(`Installing Vundle.`)
	bundleDir := path.Join(homeDir, `.vim`, `bundle`)
	err := os.MkdirAll(bundleDir, 0755)
	if err != nil {
		log.Fatal(err)
	}
	vundleDir := path.Join(bundleDir, `Vundle.vim`)
	cmd := exec.Command(`git`, `clone`, `https://github.com/VundleVim/Vundle.vim.git`, vundleDir)
	if err = cmd.Run(); err != nil {
		log.Fatal(err)
	}
}

func installPathogen() {
	// Install Pathogen
	log.Print(`Installing Pathogen.`)
	autoloadDir := path.Join(homeDir, `.vim`, `autoload`)
	err := os.MkdirAll(autoloadDir, 0755)
	if err != nil {
		log.Fatal(err)
	}
	pathogenPath := path.Join(autoloadDir, `pathogen.vim`)
	cmd := exec.Command(`curl`, `-LSso`, pathogenPath, `https://tpo.pe/pathogen.vim`)
	if err = cmd.Run(); err != nil {
		log.Fatal(err)
	}
	output, _ := cmd.Output()
	log.Print(string(output))
}

func installColors() {
	// Install some colors
	log.Print(`Installing color scheme.`)
	colorDir := path.Join(homeDir, `.vim`, `colors`)
	err := os.MkdirAll(colorDir, 0755)
	if err != nil {
		log.Fatal(err)
	}
	monokaiBytes, err := fetchFile(monokaiURL)
	if err != nil {
		log.Fatal(err)
	}
	file, err := os.OpenFile(path.Join(homeDir, `.vim`, `colors`, `monokai.vim`), os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0755)
	if err != nil {
		log.Fatal(err)
	}
	_, err = file.Write(monokaiBytes)
	if err != nil {
		log.Fatal(err)
	}
	distinguishedBytes, err := fetchFile(distinguishedURL)
	if err != nil {
		log.Fatal(err)
	}
	file, err = os.OpenFile(path.Join(homeDir, `.vim`, `colors`, `distinguished.vim`), os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0755)
	if err != nil {
		log.Fatal(err)
	}
	_, err = file.Write(distinguishedBytes)
	if err != nil {
		log.Fatal(err)
	}
}

func installPlugins() {
	// Create vim temp dir
	log.Print(`Installing plugins.`)
	err := os.MkdirAll(path.Join(homeDir, `.vim`, `temp`), 0755)
	if err != nil {
		log.Fatal(err)
	}

	// Install plugins
	cmd := exec.Command(`vim`, `-c`, `silent`, `-c`, `PluginInstall`, `-c`, `qa`)
	if err := cmd.Run(); err != nil {
		log.Fatal(err)
	}
	output, _ := cmd.Output()
	log.Print(string(output))
}

func fetchFile(url string) (data []byte, err error) {
	response, err := http.Get(url)
	if err != nil {
		return
	}
	data, err = ioutil.ReadAll(response.Body)
	response.Body.Close()
	return
}
