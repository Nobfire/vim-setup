if filereadable("~/.vimrc.before")
  r ~/.vimrc.before " Load user before file
endif

set nocompatible              " be iMproved, required
filetype off                  " required
execute pathogen#infect()

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
"" Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" Git plugin not hosted on GitHub
"" Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
"" Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
"" Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Install L9 and avoid a Naming conflict if you've already installed a
" different version somewhere else.
" Plugin 'ascenator/L9', {'name': 'newL9'}
Plugin 'VundleVim/Vundle.vim'               " Vundle
Plugin 'scrooloose/nerdtree'                " NERD Tree
Plugin 'vim-syntastic/syntastic'            " Syntastic
Plugin 'airblade/vim-gitgutter'             " Git Gutter
Plugin 'tpope/vim-fugitive'                 " Fugitive: Git wrapper
Plugin 'ervandew/supertab'                  " Supertab: Code completion
Plugin 'chrisbra/NrrwRgn'                   " Narrow Region
Plugin 'ctrlpvim/ctrlp.vim'                 " Ctrl P
Plugin 'jeetsukumaran/vim-buffergator'      " Buffergator
Plugin 'terryma/vim-multiple-cursors'       " Multi-cursor edit
Plugin 'bronson/vim-trailing-whitespace'    " Trailing Whitespace
Plugin 'ddollar/nerdcommenter'              " NERD Commenter
Plugin 'fatih/vim-go'                       " VIM-Go
Plugin 'sjl/gundo.vim'                      " Better undo
Plugin 'chiel92/vim-autoformat'             " Automatically format source code
Plugin 'dracula/vim', { 'name': 'dracula' } " Dracula color scheme
Plugin 'rafi/awesome-vim-colorschemes'      " A lot of color schemes
Plugin 'maxmellon/vim-jsx-pretty'           " JSX syntax support
Plugin 'pprovost/vim-ps1'                   " Snippets and syntax hl for powershell
Plugin 'isruslan/vim-es6'                   " Snippets and syntax hl for es6
Plugin 'pangloss/vim-javascript'            " Snippets and syntax hl for javascript
Plugin 'cakebaker/scss-syntax.vim'          " Syntax hl for scss
Plugin 'gorodinskiy/vim-coloresque'         " CSS color preview
Plugin 'tpope/vim-unimpaired'
Plugin 'garbas/vim-snipmate'                " SnipMate aims to provide support for textual snippets, similar to TextMate
Plugin 'vim-scripts/ZoomWin'                " Zoom in on current pane
Plugin 'tomtom/tlib_vim'                    " This library provides some utility functions. API for other plugins
Plugin 'MarcWeber/vim-addon-mw-utils'       " This library provides some utility functions. API for other plugins
Plugin 'chrisbra/csv.vim'                   " Pretty view CSV files in VIM
Plugin 'applescript.vim'                    " Syntax file for AppleScript
Plugin 'christianrondeau/vim-base64'        " Plugin for encoding and decoding base64
Plugin 'raimondi/delimitmate'               " This plug-in provides automatic closing of quotes, parenthesis, brackets, etc.
Plugin 'itchyny/lightline.vim'              " A light and configurable statusline/tabline plugin for Vim
Plugin 'martinda/jenkinsfile-vim-syntax'    " Jenkinsfile vim syntax
Plugin 'udalov/kotlin-vim'
Plugin 'alfredodeza/jacinto.vim'
Plugin 'majutsushi/tagbar'
Plugin 'pseewald/vim-anyfold'
if filereadable("~/.vimrc.plugin")
  r ~/.vimrc.plugin                           " Load user specified plugins
endif

call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Put your non-Plugin stuff after this line
set hlsearch     " Highlight search results
set backspace=2  " Set backspace behavior
set expandtab    " Use spaces for tabs
set tabstop=2    " Show tabs as 2 spaces
set shiftwidth=2
set autoindent   " Autoindent on newline
set smartindent  " Autoindent based on syntax
set number       " Show line numbers
set nowrap       " Do not wrap text
set mouse=a      " Enable full mouse support

" REMBER CODE FOLDING
augroup remember_folds
  autocmd!
  autocmd BufWinLeave ?* mkview 1
  autocmd BufWinEnter ?* silent! loadview 1
augroup END

" python3 setup
let g:python3_host_prog='/usr/local/bin/python3'

" Home goes to first char on line
map <Home> ^
imap <Home> <Esc>^i

" Close VIM when NERDTree is the only open pane
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif

" Shrug
hi Mornal ctermbg=none

" Indentation guides
set conceallevel=1
let g:indentLine_conceallevel=1
let g:indentLine_color_term = 59
let g:indentLine_char = '┆'
set list lcs=tab:\┆\ 

set backupcopy=yes
let g:NERDSpaceDelims = 1
let g:NERDCommentEmptyLine = 1
let g:NERDDefaultAlign = 'left'

" Map paste mode to F2
set pastetoggle=<F2>

" Map ABC sorting to F1
map <F1> :sort u<ENTER>
imap <F1> :sort u<ENTER>


" Turn on syntax highlighting and set the colors
"  colorschemes: distinguished, monokai, dracula
syntax on
set bg=dark
let g:gruvbox_contrast_dark = 'hard'
colorscheme gruvbox
"  au FileType javascript colorscheme gruvbox

" Set <leader>
let mapleader = "\\"

" NERDTree
let NERDTreeIgnore=['\.pyc$', '\.pyo$', '\.rbc$', '\.rbo$', '\.class$', '\.o$', '\~$']
map <leader>n :NERDTreeToggle<CR>:NERDTreeMirror<CR>
let g:NERDTreeWinPos = "right"

" vim-autoformat
let g:formatters_javascript = ['custom_prettier']
let g:formatters_scss = ['prettier']
let g:formatdef_custom_prettier = '"prettier --arrow-parens avoid --no-semi --single-quote --jsx-bracket-same-line --stdin-filepath ".expand("%:p").(&textwidth ? " --print-width ".&textwidth : "")." --tab-width=".shiftwidth()'
let g:autoformat_verbosemode=0
au BufWritePre *.js,*.scss Autoformat

" Fugitive
nmap <leader>gb :Gblame<CR>
nmap <leader>gs :Gstatus<CR>
nmap <leader>gd :Gdiff<CR>
nmap <leader>gl :Glog<CR>
nmap <leader>gc :Gcommit<CR>
nmap <leader>gp :Git push<CR>

" Syntastic
let g:syntastic_mode_map = {'mode': 'active'}

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0

let g:syntastic_warning_symbol = 'W>'
let g:syntastic_error_symbol = 'E>'
let g:syntastic_style_warning_symbol = 'W}'
let g:syntastic_style_error_symbol = 'E}'

let g:syntastic_stl_format = '[%E{E:%e(#%fe)}%B{,}%W{W:%w(#%fw)}]'

let g:syntastic_auto_jump = 0

let g:syntastic_java_checkers = ['checkstyle', 'javac']
let g:syntastic_java_checkstyle_classpath = '/Users/cn154515/bin/checkstyle-8.29-all.jar'
let g:syntastic_java_checkstyle_conf_file = '/Users/cn154515/.vim/checkstyle.xml'
let g:syntastic_go_checkers = ['go', 'golangci_lint', 'golint']
let g:syntastic_go_golangci_lint_fname = ""
let g:syntastic_javascript_checkers = ['eslint']
let g:syntactic_scss_checkers = ['scss_lint']

let g:syntastic_debug = 0

" vim-go
" let g:go_gmt_fail_silently = 0
let g:go_highlight_string_spellcheck = 1
let g:go_fmt_fail_silently = 0
let g:go_metalinter_enabled = 0
let g:go_metalinter_autosave = 0
let g:go_metalinter_deadline = "30s"
let g:go_fmt_command = "goimports"
let g:go_version_warning = 0

" NarrowRegion
map <leader>rn :NarrowRegion<CR>

" ctrlP
map <D-t> :CtrlP<CR>
imap <D-t> <ESC>:CtrlP<CR>

" NERDCommenter
map <leader>/ <plug>NERDCommenterToggle<CR>

" ZoomWin
map <leader>zw :ZoomWin<CR>

" Tagbar
nmap <F8> :TagbarToggle<CR>

" SnipMate
let g:snipMate = { 'snippet_version' : 1 }

if filereadable("~/.vimrc.after")
  r ~/.vimrc.after                           " Load user specified config after standard config
endif
